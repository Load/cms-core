<?php

namespace Ss;

use Phalcon\Config\Adapter\Yaml;

class Config {
	protected $dirs = array();
	protected $cache = array();
	protected $moduleName = null;

	/**
	 * @param null $defaultModuleName
	 */
	public function setDefaultModuleName($defaultModuleName) {
		$this->defaultModuleName = $defaultModuleName;
	}

	/**
	 * @return null
	 */
	public function getDefaultModuleName() {
		return $this->defaultModuleName;
	}

	/**
	 * @param null $moduleName
	 */
	public function setModuleName($moduleName) {
		$this->moduleName = $moduleName;
	}

	/**
	 * @return null
	 */
	public function getModuleName() {
		return $this->moduleName;
	}
	protected $defaultModuleName = null;

	public function setDirs(array $dirs, $merge = false) {
		if($merge) {
			$dirs = $this->dirs + $dirs;
		}

		$this->dirs = $dirs;
	}

	public function __get($name) {
		if($this->moduleName === null) {
			$this->moduleName = $this->defaultModuleName;
		}

		if(!isset($this->cache[$name])) {
			$this->cache[$this->moduleName][$name] = $this->load($this->moduleName, $name);
		}

		return $this->cache[$this->moduleName][$name];
	}

	protected function load($moduleName, $name) {
		//jeśli config o podanej nazwie nie istnieje zwracamy pusty obiekt
		if(empty($this->dirs[$moduleName])) {
			return new \Phalcon\Config();
		}

		//tworzymy config łącząc różne pomniejsze configi
		$config = new \Phalcon\Config();

		foreach($this->dirs[$moduleName] as $dir) {
			$configPath = $dir.$name.'.yaml';

			if(file_exists($configPath) && filesize($configPath) !== 0) {
				$configTemp = new Yaml($configPath);

				$config->merge($configTemp);
			}
		}

		return $config;
	}

	protected function loadold($name) {
		//ścieżka do configu
		$path = $this->path;

		/** @var $env \Ss\Env */
		$env = $this->di->get('env');

		//pobieramy wszystkie env jakie są dostępne dla plaikacji
		$childrenEnvs = $env->getChildrens();
		$childrenEnvs = array_reverse($childrenEnvs);

		//tworzymy config łącząc różne pomniejsze configi
		$config = new \Phalcon\Config();

		foreach($childrenEnvs as $childrenEnv) {
			$configPath = $path.$childrenEnv.'/'.$name.'.yaml';

			if(file_exists($configPath) && filesize($configPath) !== 0) {
				$configTemp = new Yaml($configPath);

				$config->merge($configTemp);
			}
		}

		return $config;
	}
}