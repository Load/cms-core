<?php

namespace Ss\Mvc;

use Ss\Config;
use Phalcon\Assets\Manager;

/**
 * @property array       modules
 * @property Config      config
 * @property Manager     assets
 */
class Controller extends \Phalcon\Mvc\Controller {
}