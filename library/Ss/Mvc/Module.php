<?php

namespace Ss\Mvc;

use Ss\Config;
use ReflectionClass;
use Phalcon\Assets\Manager;
use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\View\Engine\Volt;
use Phalcon\Mvc\ModuleDefinitionInterface;

/**
 * @property Router      router
 */
class Module implements ModuleDefinitionInterface {
	protected $moduleName = null;
	protected $modulePath = null;

	public function registerAutoloaders($di) {
		$className     = explode('\\', get_class($this));
		$namespaceName = $className[0];

		/** @var $dispatcher Dispatcher */
		$dispatcher = $di->get('dispatcher');

		$dispatcher->setDefaultNamespace($namespaceName.'\Controllers');

		$di->set('dispatcher', $dispatcher);
	}

	public function registerServices($di) {
		//config
		/** @var $config Config */
		$config = $di->get('config');
		$config->setModuleName($this->getModuleName());

		$di->set('config', $config);

		//volt
		$di->set('volt', function ($view, $di) {
			$volt = new Volt($view, $di);

			$cacheViewPath = $this->getModulePath().'cache/views/';

			if(!file_exists($cacheViewPath)) {
				mkdir($cacheViewPath, 0777, true);
			}

			$volt->setOptions(array(
				'compiledPath'      => $cacheViewPath,
				'compiledExtension' => '.compiled'
			));

			return $volt;
		});

		//Setup the view component
		$di->set('view', function () {
			$view = new View();

			$cacheViewPath = $this->getModulePath().'views/';

			$view->setViewsDir($cacheViewPath);

			$view->registerEngines(array(
				'.twig' => 'volt'
			));

			$view->setMainView('main');
			$view->setLayoutsDir('layouts/');

			return $view;
		});

		//
		$di->set('assets', function () use ($di) {
			$assetsConfig = $di->get('config')->assets;

			$assets = new Manager();

			foreach((array) $assetsConfig as $groupName => $group) {
				$asset = $assets->collection($groupName);

				//dodane by nie trzeba było ręcznie w każdej ścieżcie podawać
				$asset->setSourcePath('public/');

				//
				$path = 'public/'.$this->getModuleName().'/cache/assets/'.$group->ext.'/final.'.$group->ext;
				$uri  = $this->getModuleName().'/cache/assets/'.$group->ext.'/final.'.$group->ext;

				//tworzy katalogi cache jeśli ich nie ma
				if(!file_exists(dirname($path))) {
					mkdir(dirname($path), 0777, true);
				}

				//ustawia ścieżki pliku w systemie i dla uri
				$asset->setTargetPath($path);
				$asset->setTargetUri($uri);

				//dodaje filtry używkownika
				foreach((array) $group->filtrs as $filter_class_name) {
					$asset->addFilter(new $filter_class_name($di));
				}
			}

			return $assets;
		});
	}

	public function getModuleName() {
		if($this->moduleName === null) {
			$moduleClassName = get_class($this);
			$moduleClassName = explode('\\', $moduleClassName);

			$this->moduleName = strtolower($moduleClassName[0]);
		}

		return $this->moduleName;
	}

	public function getModulePath() {
		if($this->modulePath === null) {
			$moduleClassName = get_class($this);

			$reflector        = new ReflectionClass($moduleClassName);
			$moduleClassPath  = $reflector->getFileName();
			$this->modulePath = dirname($moduleClassPath).'/';
		}

		return $this->modulePath;
	}

	static function routing(Router $router) {
		return $router;
	}
}