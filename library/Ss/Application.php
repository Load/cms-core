<?php

namespace Ss;

use PDOException;
use Phalcon\Loader;
use Phalcon\Di;
use Phalcon\Exception;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Http\Response;
use Phalcon\Mvc\Url;
use Phalcon\Escaper;

class Application extends \Phalcon\Mvc\Application {
	protected $modules = array(
		'frontend' => array(
			'name'      => 'Frontend',
			'namespace' => 'Frontend\\',
			'dir'       => 'modules/frontend/',
		),
		'backend'  => array(
			'name'      => 'Backend',
			'namespace' => 'Backend\\',
			'dir'       => 'modules/backend/',
		),
	);

	protected $defaultModuleName = 'frontend';

	/**
	 * @param array $modules
	 */
	public function setModules($modules) {
		$this->modules = $modules;
	}

	/**
	 * @return array
	 */
	public function getModules() {
		return $this->modules;
	}

	/**
	 * @param string $defaultModuleName
	 */
	public function setDefaultModuleName($defaultModuleName) {
		$this->defaultModuleName = $defaultModuleName;
	}

	/**
	 * @return string
	 */
	public function getDefaultModuleName() {
		return $this->defaultModuleName;
	}

	public function start() {
		try {
			//
			$loader = new Loader();

			$loader->registerDirs(array(
				'core/library',
			));

			foreach($this->modules as $module) {
				$namespaces[$module['name']] = $module['dir'];
			}

			if(!empty($namespaces)) {
				$loader->registerNamespaces($namespaces);
			}

			$loader->register();

			//
			$application = new Application();

			//
			$di = new DI();

			//tryby w jakich będzie działać aplikacjia
			$di->set('env', function () {
				$envs = array(
					'development',
					'testing',
					'staging',
					'production',
				);

				$env = new Env($envs, 'production');

				return $env;
			});

			//config
			$di->set('config', function () use ($di) {
				$config = new Config($di);
				$config->setDefaultModuleName($this->defaultModuleName);

				foreach($this->modules as $moduleKey => $module) {
					/** @var $env Env */
					$env  = $di->get('env');
					$envs = $env->getChildrens();

					$envs = array_reverse($envs);

					//
					$configDirs[$moduleKey] = array();

					//
					$defaultConfigs = array(
						'core/config/',
						'config/',
					);

					foreach($defaultConfigs as $defaultConfig) {
						foreach($envs as $env) {
							$configDirs[$moduleKey][] = $defaultConfig.$env.'/';
						}
					}

					foreach($envs as $env) {
						$configDirs[$moduleKey][] = $module['dir'].'config/'.$env.'/';
					}

					$config->setDirs($configDirs, true);
				}

				return $config;
			});

			//
			$di->set('bootstrap', $this);

			//
			$di->set('router', function () {
				$router = new Router();

				$router->setDefaultModule($this->defaultModuleName);

				foreach($this->modules as $module) {
					$moduleClassName = '\\'.$module['namespace'].'Module';

					/** @var $moduleClassName Module */
					$router = $moduleClassName::routing($router);
				}

				return $router;
			});

			//
			$di->set('dispatcher', function () {
				$dispatcher = new Dispatcher();

				return $dispatcher;
			});

			//
			$di->set('response', function () {
				$response = new Response();

				return $response;
			});

			//
			$di->set('url', function () {
				$url = new Url();
				$url->setBaseUri('/');

				return $url;
			});

			//escaper
			$di->set('escaper', function () {
				$escaper = new Escaper();

				return $escaper;
			});

			//
			$application->setDI($di);

			//
			$modules = array();

			foreach($this->modules as $moduleKey => $module) {
				$modules[$moduleKey] = array(
					'className' => $module['namespace'].'Module',
					'path'      => $module['dir'].'Module.php',
				);
			}

			$application->registerModules($modules);

			//
			return $application->handle()->getContent();
		} catch(Exception $e) {
			return $e->getMessage();
		} catch(PDOException $e) {
			return $e->getMessage();
		}
	}
}


