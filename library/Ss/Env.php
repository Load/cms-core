<?php

namespace Ss;

use Phalcon\Http\Client\Exception;

class Env {
	protected $default = null;

	protected $envs = array();

	protected $env = false;

	public function __construct(array $envs, $defaultEnv) {
		if(empty($envs)) {
			throw new Exception('$envs is empty');
		}

		if(empty($defaultEnv)) {
			throw new Exception('$defaultEnv is empty');
		}

		if(!in_array($defaultEnv, $envs)) {
			throw new Exception('$defaultEnv there is no $envs');
		}

		$this->envs    = $envs;
		$this->default = $defaultEnv;

		if(!empty($_SERVER['env'])) {
			$this->env = $_SERVER['env'];
		}
	}

	public function setDefault($env) {
		$this->default = $env;
	}

	/**
	 * @return string
	 */
	public function get() {
		//todo dodać sprawdzanie istnienia env w envs

		if($this->env === false) {
			return $this->default;
		}

		if(!in_array($this->env, $this->envs)) {
			return $this->default;
		}

		return $this->env;
	}

	public function getAll() {
		return $this->envs;
	}

	public function getChildrens($env = false) {
		//jeśli env jest pusty używamy aktualnego
		if($env === false) {
			$env = $this->env;
		}

		//obliczamy pozycję env w array'u envs
		$idEnv = array_search($env, $this->envs);

		//wybieramy wszystkie env występujące na pozycji $idEnv i dalszych
		$childrenEnvs = array_slice($this->envs, $idEnv);

		return $childrenEnvs;
	}

	public function __toString() {
		return $this->get();
	}
}